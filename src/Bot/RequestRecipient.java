package Bot;

public class RequestRecipient {
	private String customer;
	private String vehicleno;
	
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getVehicleNo() {
		return vehicleno;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleno = vehicleNo;
	}
	public String getMumbleUser()
	{
		return String.format("%s_%s", customer, vehicleno);
	}
	
	public RequestRecipient()
	{
		
	}
	
	public RequestRecipient(String customer, String vehiclenumber)
	{
		this.customer = customer;
		this.vehicleno = vehiclenumber;
	}
}
