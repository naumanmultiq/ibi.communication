// **********************************************************************
//
// Copyright (c) 2003-2016 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.6.3
//
// <auto-generated>
//
// Generated from file `Murmur.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package Murmur;

public final class UserInfoMapHolder extends Ice.Holder<java.util.Map<UserInfo, java.lang.String>>
{
    public
    UserInfoMapHolder()
    {
    }

    public
    UserInfoMapHolder(java.util.Map<UserInfo, java.lang.String> value)
    {
        super(value);
    }
}
