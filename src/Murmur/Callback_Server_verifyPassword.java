// **********************************************************************
//
// Copyright (c) 2003-2016 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.6.3
//
// <auto-generated>
//
// Generated from file `Murmur.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package Murmur;

/**
 * Verify the password of a user. You can use this to verify a user's credentials.
 **/

public abstract class Callback_Server_verifyPassword
    extends IceInternal.TwowayCallback implements Ice.TwowayCallbackIntUE
{
    public final void __completed(Ice.AsyncResult __result)
    {
        ServerPrxHelper.__verifyPassword_completed(this, __result);
    }
}
