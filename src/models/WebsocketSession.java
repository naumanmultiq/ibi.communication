package models;

import javax.websocket.Session;

public class WebsocketSession 
{
	private String message;
	private Session session;
	private String username;
	
	public String getMessage()
	{
		return message;
	}
	
	public void setMessage(String value)
	{
		message = value;
	}
	
	public Session getSession()
	{
		return session;
	}
	
	public void setSession(Session value)
	{
		session = value;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
