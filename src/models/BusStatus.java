package models;

public class BusStatus {
	private String customerId;
	private String vehicleNumber;
	private String lastpingtime;
	private boolean isonline;
	private boolean isupdated;
	
	public BusStatus()
	{
		
	}
	
	public BusStatus(String customerid, String vehiclenumber, String lastpingtime, boolean isonline)
	{
		this.customerId = customerid;
		this.vehicleNumber = vehiclenumber;
		this.lastpingtime = lastpingtime;
		this.isonline = isonline;
		this.isupdated = true;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getLastpingtime() {
		return lastpingtime;
	}
	public void setLastpingtime(String lastpingtime) {
		this.lastpingtime = lastpingtime;
	}
	public boolean isOnline() {
		return isonline;
	}
	public void setIsOnline(boolean isonline) {
		this.isonline = isonline;
	}

	public boolean isUpdated() {
		return isupdated;
	}

	public void setIsupdated(boolean isupdated) {
		this.isupdated = isupdated;
	}
	
	
}
