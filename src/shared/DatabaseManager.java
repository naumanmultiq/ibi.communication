package shared;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.mina.core.session.IoSession;

import models.BusStatus;

public class DatabaseManager {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = AppSettings.isStaging()?"jdbc:mysql://localhost/communicationstaging":"jdbc:mysql://localhost/communication";

	// Database credentials
	static final String USER = AppSettings.isStaging()?"staging":"communication";
	static final String PASS = AppSettings.isStaging()?"staging123":"ibi0786";

	public static void addEndCallLog(String from, String to, int customerId) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			String sql = "UPDATE calls set callstatus=2 where source='" + from + "' and dest='" + to
					+ "' and customerid='" + customerId + "'";
			stmt.executeUpdate(sql);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
			} // end finally try
		}
	}

	public static void addStartCallLog(String from, String to, int customerId, String channel) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			String sql = "INSERT INTO calls (`customerid` ,`src` ,`dest` ,`type` ,`channel`) VALUES (" + customerId + ", '"
					+ from + "',  '" + to + "',  1,  '" + channel + "')";
			stmt.executeUpdate(sql);
		} catch (Exception ex) {
			Logger.write("communication_bot", "call to db " + ex.getMessage());
			Logger.writeError("communication_bot", ex);
		} finally {
			// finally block used to close resources
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
			} // end finally try
		}
	}

	public static String authenticate(String userName, String password) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String token = "";
		try {
			Logger.write("communication_rest", "Authentication Started");

			// STEP 1: Register JDBC driver

			Class.forName(JDBC_DRIVER);
			Logger.write("communication_rest", "Step -01 Register JDBC driver");

			// STEP 2: Open a connection
			// System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			Logger.write("communication_rest", "Step -02 Open a connection");

			// STEP 3: Execute a query
			// System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT * FROM users WHERE username='" + userName + "' and password='" + password + "'";
			Logger.write("communication_rest", sql);
			rs = stmt.executeQuery(sql);
			Logger.write("communication_rest", "Qeury executed");
			// STEP 4: Extract data from result set
			while (rs.next()) {
				Logger.write("communication_rest", "User has been authenticated");
				token = issueToken();
			}
			// STEP 6: Clean-up environment
			// rs.close();
			// stmt.close();
			// conn.close();
		} catch (SQLException se) {
			Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			Logger.writeError("communication_rest", se);
		} catch (Exception e) {
			// return "NOT AUTH";
			Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			Logger.writeError("communication_rest", e);
		} finally {
			// finally block used to close resources
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException se2) {
				Logger.writeError("communication_rest", se2);
			} // nothing we can do
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
				Logger.writeError("communication_rest", se2);
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				Logger.writeError("communication_rest", se);
			} // end finally try
		} // end try
		return token;
	}

	private static String issueToken() {
		Random random = new SecureRandom();
		String token = new BigInteger(130, random).toString(32);
		return token;
	}

	public static void login(String username, String password) {
		Connection conn = null;
		try {
			conn = DriverManager
					.getConnection("jdbc:mysql://localhost/communication?" + "user=communication&password=ibi0786");
			System.out.println("connection establish");
			// Do something with the Connection
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}

	public static List<BusStatus> getBusStatusList() {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<BusStatus> result = new ArrayList<BusStatus>();
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			String sql = "SELECT * FROM vehicle";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				// result.add(new BusStatus())
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
			} // end finally try
		}
		return null;
	}

	public static boolean addPing(String customerid, String vehicleNumber, String line, String lat, String lng, String destination, String patjourneyid, String journeytype) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		boolean gpsUpdated = true;
		try {
			if (lat == null){
				lat = "";
			}
			if (lng == null){
				lng = "";
			}
			if (destination == null){
				destination = "";
			}
			if (patjourneyid == null){
				patjourneyid = "";
			}
			if (journeytype == null){
				journeytype = "";
			}
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			//Logger.write("communication_bot", "Check if customer already exists");
			String customerSQL = "select * from customer where customerid=" + customerid;
			rs = stmt.executeQuery(customerSQL);
			boolean bExist = rs.next();
			rs.close();
			if (!bExist) {
				//Logger.write("communication_bot", "Customer does not exist so add customer");
				customerSQL = "INSERT INTO customer (customerid, customername) VALUES (" + customerid + ",'"
						+ customerid + "')";
				stmt.executeUpdate(customerSQL);
				//Logger.write("communication_bot", "New customer added");
			}
			String sql = "SELECT * FROM vehicle WHERE customerid=" + customerid + " and vehiclenumber='" + vehicleNumber
					+ "'";
			rs = stmt.executeQuery(sql);
			//Logger.write("communication_bot", "Chekc if the vehicle exists");
			bExist = rs.next();
			if (bExist) {
				//Logger.write("communication_bot", "Vehicle exists - update record");
				//Logger.write("communication_bot", "the vehicle exists " + customerid + ", " + vehicleNumber);
				//if (lat != null && !lat.isEmpty() && lng!=null && !lng.isEmpty()) {
					//Logger.write("communication_bot", "updating last ping time with lat and long");
					sql = "UPDATE vehicle set lastpingtime=NOW(),lat='" + lat + "',lng='" + lng + "',line='" + line
							+ "',destination='" + destination + "',PTAjourneyId='" + patjourneyid + "',journeytype='" + journeytype + "' where customerid=" + customerid + " and vehiclenumber='" + vehicleNumber + "'";

				//} else {
					//Logger.write("communication_bot", "updating last ping time with no lat and long");
				//	sql = "UPDATE vehicle set lastpingtime=NOW() where customerid=" + customerid
				//			+ " and vehiclenumber='" + vehicleNumber + "'";
				//}
				String dbLat = rs.getString("lat");
				String dbLong = rs.getString("lng");
				if (dbLat!=null && dbLat.equalsIgnoreCase(lat) && dbLong != null && dbLong.equalsIgnoreCase(lng))
					gpsUpdated = false;
				stmt.executeUpdate(sql);
				
				
			} else {
				//Logger.write("communication_bot", "This is new vehicle" + customerid + ", " + vehicleNumber);
				//if (!lat.isEmpty() && !lng.isEmpty()) {
					//Logger.write("communication_bot", "Adding vehicle info with no lat long");
					sql = "INSERT into vehicle (customerid, vehiclenumber, lastpingtime,lat,lng,line,destination,PTAjourneyId,journeytype) VALUES ("
							+ customerid + ", '" + vehicleNumber + "',NOW(),'" + lat + "','" + lng + "','" + line + "','"+destination+"','"+patjourneyid+"','"+journeytype+"')";
				//} else {
				//	Logger.write("communication_bot", "Adding vehicle info with lat long");
				//	sql = "INSERT into vehicle (customerid, vehiclenumber, lastpingtime,line) VALUES (" + customerid
				//			+ ", '" + vehicleNumber + "',NOW(),'" + line + "')";
				//}

				stmt.executeUpdate(sql);
				
				sql = "INSERT into pinghistory (customerid, vehiclenumber, lastpingtime,lat,lng,line) VALUES ("
						+ customerid + ", '" + vehicleNumber + "',NOW(),'" + lat + "','" + lng + "','" + line+ "')";
				stmt.executeUpdate(sql);
				//Logger.write("communication_bot", "Ping updated");
			}
		} catch (Exception ex) {
			// ex.printStackTrace();
			Logger.write("communication_bot", "Exception in Ping " + ex.getMessage());
			Logger.writeError("communication_bot", ex);
		} finally {
			// finally block used to close resources
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
			} // end finally try
		}
		return gpsUpdated;
	}

	public static List<Vehicle> getVehicleList() {
		return null;
	}

	public static void addCallData(String type, String customerId, String from, String to, String channel,
			String status) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();

			String sql = "INSERT into calls (type, customerid, src, dest,channel, status) VALUES ('" + type + "', '"
					+ customerId + "','" + from + "','" + to + "','" + channel + "','" + status + "')";
			Logger.write("communication_bot", sql);
			stmt.executeUpdate(sql);
		} catch (Exception ex) {
			Logger.write("communication_bot", "call to db " + ex.getMessage());
			Logger.writeError("communication_bot", ex);
		} finally {
			// finally block used to close resources
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
			} // end finally try
		}

	}

	private static Connection conn = null;
	private static CallableStatement stmt = null;
	private static ResultSet rs = null;

	private static Connection getDBConnection() {
		Connection conn = null;
		try {
			// STEP 1: Register JDBC driver
			Class.forName(JDBC_DRIVER);
			Logger.write("communication_rest", "Step -01 Register JDBC driver");

			// STEP 2: Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			Logger.write("communication_rest", "Step -02 Open a connection");

			return conn;
		} catch (SQLException se) {
			Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			Logger.writeError("communication_rest", se);
		} catch (Exception e) {
			Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			Logger.writeError("communication_rest", e);
		}
		return null;
	}

	public static Map<String, String> busStatuses = new HashMap<String, String>();

	public static boolean isPanicEnabled(String customerId, String busNumber){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
	
		try{
			conn = getDBConnection();
			stmt = conn.createStatement();
			String qry = "SELECT panicallowed FROM vehicle where customerid='" + customerId + "' and vehiclenumber='" + busNumber + "'";
			Logger.write("communication_bot", qry);
			rs = stmt.executeQuery(qry);
			if (rs.next()) {
				int panicallowed = rs.getInt("panicallowed");
				if (panicallowed == 0){
					return false;
				}
			}
		}catch(Exception ex){
			Logger.write("communication_bot", ex.getMessage());
			Logger.writeError("communication_bot", ex);
		}
		return true;
	}
	public static Boolean IsOperatorCanReceiveCall(String userName)
    {
	   Boolean ret = false;
	   try
	   {
		   try
		   {
		      conn = getDBConnection();
		      Logger.write("communication_bot_DatabaseManage", "Step -02 Open a connection");
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call isoperatorcanreceivecall(?)}");
		      stmt.setString(1, userName);
		      rs = stmt.executeQuery();
		      Logger.write("communication_bot_DatabaseManage", "Qeury executed");
		      //STEP 4: Extract data from result set
		      while(rs.next())
		      {
		    	String auth = rs.getString("canrecieve");
		    	if (auth.equalsIgnoreCase("1"))
		    		ret = true;
		    		//return ApplicationUtility.issueToken();
		      }
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_bot_DatabaseManage", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_bot_DatabaseManage",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_bot_DatabaseManage", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_bot_DatabaseManage",  e);
		   }
		   finally
		   {
			   cleanup();
		   }//end try
	   }
	   catch(Exception se)
	   {
    	  Logger.writeError("communication_bot_DatabaseManage",  se);
	   }//end finally try
	   return ret;
   }
	public static String vehicleListWithStatus(int groupId) {
		try {
			conn = getDBConnection();
			Logger.write("communication_rest", "Step -02 Open a connection");

			// STEP 3: Execute a query
			stmt = conn.prepareCall("{call getUpdatedVehicleStatus(?)}");
			stmt.setInt(1, 60);
			// stmt.setInt(2, ClientConfigurations.getAgeToOffline()); // Need
			// to pass configuration of offlineage
			// stmt.setInt(2, 1); // Need to pass configuration of offlineage
			rs = stmt.executeQuery();
			Logger.write("communication_status", "Qeury executed");
			// STEP 4: Extract data from result set
			String finalJson = "";

			while (rs.next()) {
				String vehicleNumber = String.format("%s_%s", rs.getString("customerid"),
						rs.getString("vehiclenumber"));
				String jsonString = "{" + "\"vehiclenumber\":\"" + rs.getString("vehiclenumber") + "\","
						+ "\"customer\":\"" + rs.getString("customerid") + "\"," 
						+ "\"currentstatus\":\"" + rs.getString("status") + "\"," + "\"line\":\"" + rs.getString("line")+ 
						"\"," + "\"destination\":\"" + rs.getString("destination")+ 
						"\"," + "\"ptajourneyid\":\"" + rs.getString("ptajourneyid")+ "\"," + "\"journeytype\":\"" + rs.getString("journeytype")+ 
						"\"" + "," + "\"groups\":\"" + rs.getString("groups")+ 
						"\"" + "}";
				if (busStatuses.containsKey(vehicleNumber)) {
					String oldData = (String) busStatuses.get(vehicleNumber);
					if (oldData != null && oldData.equalsIgnoreCase(jsonString)) {
						continue;
					}
				}
				busStatuses.put(vehicleNumber, jsonString);
				if (!finalJson.isEmpty())
					finalJson += ",";
				finalJson += jsonString;
			}
			if (!ApplicationUtility.isNullOrEmpty(finalJson))
				finalJson = "{\"command\":\"vehiclelist\", \"vehicles\":" + "[" + finalJson + "]" + "}";
			Logger.write("communication_status", "Returned " + finalJson);
			return finalJson;
			// STEP 6: Clean-up environment
		} catch (SQLException se) {
			Logger.write("communication_status", "Exception DB: " + se.getMessage());
			Logger.writeError("communication_status", se);
		} catch (Exception e) {
			// return "NOT AUTH";
			Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			Logger.writeError("communication_rest", e);
		} finally {
			cleanup();
		} // end try
		return "{}";
	}
	
	public static List<String> getUniqueMumbleServers() {
		List<String> mumbleHosts = new ArrayList<String>();
		try {
			conn = getDBConnection();

			// STEP 3: Execute a query
			stmt = conn.prepareCall("{call getDistinctMumbleServers()}");
			rs = stmt.executeQuery();
			Logger.write("communication_status", "Qeury executed");
			while (rs.next()) {
				mumbleHosts.add(String.format("%s;$s", rs.getString("mumblehost"), rs.getString("mumbleport")));
			}
		} catch (SQLException se) {
			Logger.write("communication_bot", "Exception DB: " + se.getMessage());
			Logger.writeError("communication_bot", se);
		} catch (Exception e) {
			Logger.write("communication_bot", "Exception DB: " + e.getMessage());
			Logger.writeError("communication_bot", e);
		} finally {
			cleanup();
		} // end try
		return mumbleHosts;
	}
	
	public static String getMumbleHost(int customerId) {
		String mumbleHost = "";
		try {
			conn = getDBConnection();

			// STEP 3: Execute a query
			stmt = conn.prepareCall("{call getHostAndPort(?)}");
			stmt.setInt(1, customerId);
			rs = stmt.executeQuery();
			Logger.write("communication_status", "Qeury executed");
			if (rs.next()) {
				mumbleHost = rs.getString("mumblehost");
			}
			return mumbleHost;
		} catch (SQLException se) {
			Logger.write("communication_bot", "Exception DB: " + se.getMessage());
			Logger.writeError("communication_bot", se);
		} catch (Exception e) {
			Logger.write("communication_bot", "Exception DB: " + e.getMessage());
			Logger.writeError("communication_bot", e);
		} finally {
			cleanup();
		} // end try
		return "{}";
	}
	
	public static String getMumblePort(int customerId) {
		String mumblePort = "";
		try {
			conn = getDBConnection();

			// STEP 3: Execute a query
			stmt = conn.prepareCall("{call getHostAndPort(?)}");
			stmt.setInt(1, customerId);
			rs = stmt.executeQuery();
			Logger.write("communication_status", "Qeury executed");
			if (rs.next()) {
				mumblePort = rs.getString("mumbleport");
			}
			return mumblePort;
		} catch (SQLException se) {
			Logger.write("communication_bot", "Exception DB: " + se.getMessage());
			Logger.writeError("communication_bot", se);
		} catch (Exception e) {
			Logger.write("communication_bot", "Exception DB: " + e.getMessage());
			Logger.writeError("communication_bot", e);
		} finally {
			cleanup();
		} // end try
		return "{}";
	}

	private static void cleanup() {
		// finally block used to close resources
		try {
			if (rs != null)
				rs.close();
		} catch (SQLException se2) {
			Logger.writeError("communication_rest", se2);
		} // nothing we can do
		try {
			if (stmt != null)
				stmt.close();
		} catch (SQLException se2) {
			Logger.writeError("communication_rest", se2);
		} // nothing we can do
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException se) {
			Logger.writeError("communication_rest", se);
		} // end finally try
	}
}